﻿#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <cstddef>
#include <string>
//comment
//comment

using namespace std;

class Animal
{

public:
	
	virtual void Voice()
	{
		cout << "Voice: " << "poof..." << endl;
	}
};
class Dog : public Animal
{
private:
	//string vD = "Woof!";
public:
	void Voice() override
	{
		cout << "Вуф!!" << endl;
	}
};

class Cat : public Animal
{

public:

	void Voice() override
	{
		cout << "Мяу!!" << endl;
	}
};

class Belsparrow : public Animal
{

public:
	
	void Voice() override
	{
		cout << "Чык-чырык!!" << endl;
	}
};

int main()
{
	setlocale(LC_ALL, "rus");
	Animal* an[3];
	an[0] = new Dog();
	an[1] = new Cat();
	an[2] = new Belsparrow();

	for (Animal* a : an)
	{
		a->Voice();
	}
}